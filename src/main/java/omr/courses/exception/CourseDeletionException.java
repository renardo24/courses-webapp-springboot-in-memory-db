package omr.courses.exception;

import omr.courses.domain.Course;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CourseDeletionException extends CourseException {

    public CourseDeletionException(final String message) {
        super(message);
    }

    public CourseDeletionException(final Course c) {
        super(c);
    }

    public CourseDeletionException(final Course c, final String message) {
        super(c, message);
    }
}
