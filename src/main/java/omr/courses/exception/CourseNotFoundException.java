package omr.courses.exception;

import omr.courses.domain.Course;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CourseNotFoundException extends CourseException {

    public CourseNotFoundException(final String message) {
        super(message);
    }

    public CourseNotFoundException(final Course c) {
        super(c);
    }

    public CourseNotFoundException(final Course c, final String message) {
        super(c, message);
    }
}
