package omr.courses.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Title is mandatory")
    private String title;

    @NotNull(message = "Start date is mandatory")
    @Column(name = "start_date")
    private Date startDate;

    @NotNull(message = "End date is mandatory")
    @Column(name = "end_date")
    private Date endDate;

    @NotBlank(message = "Title is mandatory")
    private String duration;

    @NotNull(message = "Course type is mandatory")
    @ManyToOne
    private CourseType type;

    @NotNull(message = "Course provider is mandatory")
    @ManyToOne
    private CourseProvider provider;

    private String comments;

    private String instructors;

    private String location;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public CourseType getType() {
        return type;
    }

    public void setType(CourseType type) {
        this.type = type;
    }

    public CourseProvider getProvider() {
        return provider;
    }

    public void setProvider(CourseProvider provider) {
        this.provider = provider;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getInstructors() {
        return instructors;
    }

    public void setInstructors(String instructors) {
        this.instructors = instructors;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", duration='" + duration + '\'' +
                ", type=" + type +
                ", provider=" + provider +
                ", comments='" + comments + '\'' +
                ", instructors='" + instructors + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
