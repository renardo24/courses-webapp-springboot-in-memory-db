package omr.courses.transformer;

import omr.courses.domain.Course;
import omr.courses.exception.CourseValidationException;
import omr.courses.model.CourseProvider;
import omr.courses.repository.CourseProviderRepository;
import omr.courses.repository.CourseTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.sql.Date;

@Component
public class CourseTransformerToCourseForDb {

    @Autowired
    private CourseTypeRepository courseTypesRepository;

    @Autowired
    private CourseProviderRepository courseProviderRepository;

    public omr.courses.model.Course transform(final Course courseFromDomain) {
        return toCourseForDB(courseFromDomain);
    }

    private omr.courses.model.Course toCourseForDB(@Valid final Course courseFromDomain) {
        try {
            omr.courses.model.Course courseForDB = new omr.courses.model.Course();
            courseForDB.setTitle(courseFromDomain.getTitle());
            courseForDB.setComments(courseFromDomain.getComments());
            courseForDB.setDuration(courseFromDomain.getDuration());
            courseForDB.setInstructors(courseFromDomain.getInstructors());
            courseForDB.setId(courseFromDomain.getId());
            courseForDB.setLocation(courseFromDomain.getLocation());
            CourseProvider courseProvider = handleCourseProvider(courseFromDomain.getProvider());
            courseForDB.setProvider(courseProvider);
            courseForDB.setEndDate(Date.valueOf(courseFromDomain.getEndDate()));
            courseForDB.setStartDate(Date.valueOf(courseFromDomain.getStartDate()));
            courseForDB.setType(courseTypesRepository.findByName(courseFromDomain.getType()
                                                                                 .getName()));
            return courseForDB;
        } catch (NullPointerException npe) {
            throw new CourseValidationException(npe.getMessage());
        }
    }

    private CourseProvider handleCourseProvider(final String provider) {
        CourseProvider courseProvider = courseProviderRepository.findByName(provider);
        if (courseProvider == null) {
            CourseProvider newCourseProvider = new CourseProvider();
            newCourseProvider.setName(provider);
            courseProvider = courseProviderRepository.save(newCourseProvider);
        }

        return courseProvider;
    }

}
