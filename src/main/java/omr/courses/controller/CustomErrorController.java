package omr.courses.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController implements ErrorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomErrorController.class);

    @RequestMapping("/error")
    public String handleError(final HttpServletRequest request) {

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        LOGGER.warn("Error exception: {}", request.getAttribute(RequestDispatcher.ERROR_EXCEPTION));
        LOGGER.warn("Error exception type: {}", request.getAttribute(RequestDispatcher.ERROR_EXCEPTION_TYPE));
        LOGGER.warn("Error status code: {}", request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));
        LOGGER.warn("Error message: {}", request.getAttribute(RequestDispatcher.ERROR_MESSAGE));
        LOGGER.warn("Error request URI: {}", request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI));
        LOGGER.warn("Error servlet name: {}", request.getAttribute(RequestDispatcher.ERROR_SERVLET_NAME));

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "error/error-404";
            } else if (statusCode == HttpStatus.BAD_REQUEST.value()) {
                return "error/error-400";
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "error/error-500";
            }
        }

        return "error/error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
