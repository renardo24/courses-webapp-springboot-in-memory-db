package omr.courses.controller;

import omr.courses.Constants;
import omr.courses.domain.Course;
import omr.courses.domain.CourseValidator;
import omr.courses.service.CourseRetrievalService;
import omr.courses.service.CourseUpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CourseUpdateController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseUpdateController.class);

    @Autowired
    private CourseRetrievalService courseRetrievalService;

    @Autowired
    private CourseValidator courseValidator;

    @Autowired
    private CourseUpdateService courseUpdateService;

    @GetMapping(value = "/update-course-page/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String goToUpdateCoursePage(@PathVariable final long id, final Model model) {
        Course course = courseRetrievalService.getCourseById(id);
        model.addAttribute("course", course);
        model.addAttribute("formPath", "update-course");
        model.addAttribute("pageTitle", "Update Existing Course");
        LOGGER.info("Retrieving page to update course");
        return Constants.COURSE_FORM_PAGE;
    }

    @PostMapping(value = "/update-course")
    public ModelAndView updateCourse(@Valid @ModelAttribute("course") final Course course) {
        LOGGER.info("Updating existing course");
        courseValidator.validateCourse(course);
        courseUpdateService.updateCourse(course);
        LOGGER.info("Existing course updated: {}", course);
        return new ModelAndView("redirect:/courses");
    }
}
