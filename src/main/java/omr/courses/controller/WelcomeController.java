package omr.courses.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class WelcomeController {

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public String index() {
        return "index";
    }

    @GetMapping("/welcome")
    @ResponseStatus(HttpStatus.OK)
    public String welcome() {
        return "index";
    }
}
