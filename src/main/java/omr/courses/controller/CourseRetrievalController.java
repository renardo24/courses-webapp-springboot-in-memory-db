package omr.courses.controller;

import omr.courses.Constants;
import omr.courses.domain.Course;
import omr.courses.service.CourseRetrievalService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Controller
public class CourseRetrievalController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseRetrievalController.class);

    @Autowired
    private CourseRetrievalService courseRetrievalService;

    @GetMapping(value = "/courses")
    @ResponseStatus(HttpStatus.OK)
    public String goToCoursesListPage(final Model model) {
        LOGGER.info("Retrieving all courses");
        List<Course> coursesList = courseRetrievalService.getCourses();
        LOGGER.info("Retrieved all courses");
        model.addAttribute("coursesList", coursesList);
        return Constants.COURSES_LIST;
    }

    @GetMapping(value = "/courses/{id}")
    @ResponseStatus(HttpStatus.OK)
    public String goToCoursePage(@PathVariable final long id, final Model model) {
        LOGGER.info("About to retrieve course with id: {}", id);
        Course course = courseRetrievalService.getCourseById(id);
        LOGGER.info("Retrieved course with id: {}", id);
        model.addAttribute("course", course);
        return Constants.COURSE_DETAILS;
    }

}
