package omr.courses.integration.controller.mockmvc_autoconfig;

import omr.courses.exception.CourseDeletionException;
import omr.courses.service.CourseDeletionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
public class CourseDeletionControllerMockMvcIntegrationTestACM {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CourseDeletionService courseDeletionService;

    @Test
    public void when_deleteCourse_expect_success() throws Exception {
        Mockito.doNothing()
               .when(courseDeletionService)
               .deleteCourseById(1L);
        mvc.perform(MockMvcRequestBuilders.post("/delete-course/1"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .is3xxRedirection())
           .andExpect(MockMvcResultMatchers.model()
                                           .size(0))
           .andExpect(MockMvcResultMatchers.redirectedUrl("/courses"));
    }

    @Test
    public void when_deleteCourseNotInDb_expect_error() throws Exception {
        Mockito.doThrow(new CourseDeletionException("Cannot delete course"))
               .when(courseDeletionService)
               .deleteCourseById(1L);
        mvc.perform(MockMvcRequestBuilders.post("/delete-course/1"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isBadRequest());
    }

}
