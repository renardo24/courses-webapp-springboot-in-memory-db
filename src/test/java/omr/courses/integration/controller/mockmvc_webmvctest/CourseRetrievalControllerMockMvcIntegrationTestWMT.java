package omr.courses.integration.controller.mockmvc_webmvctest;

import omr.courses.controller.CourseRetrievalController;
import omr.courses.domain.Course;
import omr.courses.exception.CourseNotFoundException;
import omr.courses.service.CourseRetrievalService;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(CourseRetrievalController.class)
public class CourseRetrievalControllerMockMvcIntegrationTestWMT {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CourseRetrievalService courseRetrievalService;

    private List<Course> createCoursesList(final int listSize) {
        List<Course> coursesList = new ArrayList<>();
        for (int i = 0; i < listSize; i++) {
            Course course = new Course("Course #" + (i + 1));
            coursesList.add(course);
        }
        return coursesList;
    }

    private Course createCourse(final String title) {
        return new Course(title);
    }

    @Test
    public void retrievalSuccess_WhenNoCoursesInDatabase() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/courses"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.model()
                                           .attributeExists("coursesList"))
           .andExpect(MockMvcResultMatchers.model()
                                           .attribute("coursesList", IsCollectionWithSize.hasSize(0)))
           .andExpect(MockMvcResultMatchers.view()
                                           .name("courses-list"));
    }

    @Test
    public void retrievalSuccess_WhenOneCourseInDatabase() throws Exception {
        List<Course> coursesList = createCoursesList(1);
        Mockito.when(courseRetrievalService.getCourses())
               .thenReturn(coursesList);
        mvc.perform(MockMvcRequestBuilders.get("/courses"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.model()
                                           .attributeExists("coursesList"))
           .andExpect(MockMvcResultMatchers.model()
                                           .attribute("coursesList", IsCollectionWithSize.hasSize(1)))
           .andExpect(MockMvcResultMatchers.model()
                                           .attribute("coursesList", Matchers.hasItem(
                                                   Matchers.hasProperty("title", Matchers
                                                           .equalTo("Course #1")))))
           .andExpect(MockMvcResultMatchers.view()
                                           .name("courses-list"));
    }

    @Test
    public void retrievalSuccess_WhenSeveralCoursesInDatabase() throws Exception {
        List<Course> coursesList = createCoursesList(3);
        Mockito.when(courseRetrievalService.getCourses())
               .thenReturn(coursesList);
        mvc.perform(MockMvcRequestBuilders.get("/courses"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.model()
                                           .attributeExists("coursesList"))
           .andExpect(MockMvcResultMatchers.model()
                                           .attribute("coursesList", IsCollectionWithSize.hasSize(3)))
           .andExpect(MockMvcResultMatchers.model()
                                           .attribute("coursesList", Matchers.hasItem(
                                                   Matchers.hasProperty("title", Matchers
                                                           .equalTo("Course #1")))))
           .andExpect(MockMvcResultMatchers.model()
                                           .attribute("coursesList", Matchers.hasItem(
                                                   Matchers.hasProperty("title", Matchers
                                                           .equalTo("Course #2")))))
           .andExpect(MockMvcResultMatchers.model()
                                           .attribute("coursesList", Matchers.hasItem(
                                                   Matchers.hasProperty("title", Matchers
                                                           .equalTo("Course #3")))))
           .andExpect(MockMvcResultMatchers.view()
                                           .name("courses-list"));
    }

    @Test
    public void when_courseByIdNotInDatabase_expect_404NotFound() throws Exception {
        Mockito.doThrow(new CourseNotFoundException("Course not found"))
               .when(courseRetrievalService)
               .getCourseById(321L);
        mvc.perform(MockMvcRequestBuilders.get("/courses/321"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isNotFound());
    }

    @Test
    public void when_courseByIdInDatabase_expect_200SuccessAndModelWithAttribute() throws Exception {
        Course course = createCourse("Course #1 Title");
        Mockito.when(courseRetrievalService.getCourseById(321))
               .thenReturn(course);
        mvc.perform(MockMvcRequestBuilders.get("/courses/321"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.model()
                                           .attributeExists("course"))
           .andExpect(MockMvcResultMatchers.view()
                                           .name("course-details"));
    }
}
