package omr.courses.integration.controller.mockmvc_webmvctest;

import omr.courses.controller.WelcomeController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(WelcomeController.class)
public class WelcomeControllerMockMvcIntegrationTestWMT {

    @Autowired
    private MockMvc mvc;

    @Test
    public void rootSuccess() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.view()
                                           .name("index"));
    }

    @Test
    public void welcomeSuccess() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/welcome"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.view()
                                           .name("index"));

    }
}
