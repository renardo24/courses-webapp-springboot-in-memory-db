package omr.courses.integration.controller.mockmvc_webmvctest;

import com.fasterxml.jackson.databind.ObjectMapper;
import omr.courses.controller.CourseCreationController;
import omr.courses.domain.Course;
import omr.courses.domain.CourseType;
import omr.courses.domain.CourseValidator;
import omr.courses.exception.CourseValidationException;
import omr.courses.service.CourseCreationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@WebMvcTest(CourseCreationController.class)
public class CourseCreationControllerMockMvcIntegrationTestWMT {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CourseCreationService courseCreationService;

    @MockBean
    private CourseValidator courseValidator;

    @Test
    public void when_getAddCoursePage_expect_success() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/add-course-page"))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isOk())
           .andExpect(MockMvcResultMatchers.model()
                                           .attributeExists("course"))
           .andExpect(MockMvcResultMatchers.model()
                                           .attributeExists("formPath"))
           .andExpect(MockMvcResultMatchers.model()
                                           .attribute("formPath", "add-course"))
           .andExpect(MockMvcResultMatchers.model()
                                           .attributeExists("pageTitle"))
           .andExpect(MockMvcResultMatchers.view()
                                           .name("course-form"));
    }

    @Test
    public void when_addCourse_expect_success() throws Exception {
        Course course = new Course("My Wonderful Course!!!");
        course.setId(1L);
        course.setStartDate(LocalDate.of(2009, 3, 12));
        course.setEndDate(LocalDate.of(2009, 3, 15));
        course.setDuration("4d");
        course.setProvider("SkillsMatter");
        course.setType(CourseType.CLASSROOM);
        Mockito.doNothing()
               .when(courseValidator)
               .validateCourse(ArgumentMatchers.eq(course));

        String wrapperJson = new ObjectMapper().writeValueAsString(course);

        mvc.perform(MockMvcRequestBuilders.post("/add-course")
                                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                                          .characterEncoding("utf-8")
                                          .flashAttr("course", course))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .is3xxRedirection())
           .andExpect(MockMvcResultMatchers.redirectedUrl("/courses"));
    }

    @Test
    public void when_addCourseWithStartDateGreaterThanEndDate_expect_400StatusCode() throws Exception {
        Course course = new Course("My Wonderful Course!!!");
        course.setId(1L);
        course.setStartDate(LocalDate.of(2009, 3, 15));
        course.setEndDate(LocalDate.of(2009, 3, 12));
        course.setDuration("4d");
        course.setProvider("SkillsMatter");
        course.setType(CourseType.CLASSROOM);
        Mockito.doThrow(new CourseValidationException("Invalid date"))
               .when(courseValidator)
               .validateCourse(course);

        String wrapperJson = new ObjectMapper().writeValueAsString(course);

        mvc.perform(MockMvcRequestBuilders.post("/add-course")
                                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                                          .characterEncoding("utf-8")
                                          .flashAttr("course", course))
           .andDo(MockMvcResultHandlers.print())
           .andExpect(MockMvcResultMatchers.status()
                                           .isBadRequest());
    }
}
