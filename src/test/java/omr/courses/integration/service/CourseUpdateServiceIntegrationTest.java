package omr.courses.integration.service;

import omr.courses.domain.Course;
import omr.courses.service.CourseRetrievalService;
import omr.courses.service.CourseUpdateService;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseUpdateServiceIntegrationTest {

    @Autowired
    private CourseUpdateService courseUpdateService;
    @Autowired
    private CourseRetrievalService courseRetrievalService;

    @Test
    public void when_addNewCourse_expect_success() {
        Course course = courseRetrievalService.getCourseById(3L);
        course.setTitle("Java 8 In The Flesh");
        courseUpdateService.updateCourse(course);

        List<Course> courses = courseRetrievalService.getCourses();
        Assertions.assertThat(courses)
                  .isNotNull();
        Assertions.assertThat(courses)
                  .isNotEmpty();

        Course theCourse = courseRetrievalService.getCourseById(3L);
        Assertions.assertThat(theCourse.getTitle())
                  .isEqualTo("Java 8 In The Flesh");
    }
}
