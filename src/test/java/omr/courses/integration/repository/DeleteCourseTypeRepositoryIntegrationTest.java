package omr.courses.integration.repository;

import omr.courses.model.CourseType;
import omr.courses.repository.CourseTypeRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DeleteCourseTypeRepositoryIntegrationTest {

    @Autowired
    private CourseTypeRepository courseTypeRepository;

    @Test
    public void when_deleteExistingCourseType_expect_success() {
        CourseType newCourseType = new CourseType();
        newCourseType.setName("SOMEGREATTYPE");
        courseTypeRepository.save(newCourseType);

        CourseType courseType = courseTypeRepository.findByName("SOMEGREATTYPE");
        courseTypeRepository.delete(courseType);
        Iterable<CourseType> courseTypes = courseTypeRepository.findAll();
        Assertions.assertThat(courseTypes)
                  .hasSize(3);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void when_deleteByIdThatDoesNotExist_expect_exception() {
        courseTypeRepository.deleteById(211L);
    }
}
