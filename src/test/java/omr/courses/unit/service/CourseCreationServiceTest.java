package omr.courses.unit.service;

import omr.courses.exception.CourseCreationException;
import omr.courses.model.Course;
import omr.courses.model.CourseType;
import omr.courses.repository.CourseRepository;
import omr.courses.service.CourseCreationService;
import omr.courses.transformer.CourseTransformerToCourseForDb;
import omr.courses.transformer.CourseTransformerToCourseForDomain;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
public class CourseCreationServiceTest {

    @MockBean
    private CourseRepository courseRepository;

    @MockBean
    private CourseTransformerToCourseForDb courseTransformerToCourseForDb;

    @MockBean
    private CourseTransformerToCourseForDomain courseTransformerToCourseForDomain;

    @InjectMocks
    private CourseCreationService courseCreationService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private omr.courses.domain.Course createDomainCourse(final String title) {
        omr.courses.domain.Course course = new omr.courses.domain.Course(title);
        course.setId(1L);
        course.setStartDate(LocalDate.now());
        course.setEndDate(LocalDate.now());
        course.setType(omr.courses.domain.CourseType.ONLINE);
        return course;
    }

    private Course createCourseForDb(final String title) {
        Course course = new Course();
        course.setId(1L);
        course.setTitle("A great course about UML!!!");
        course.setStartDate(new Date(System.currentTimeMillis()));
        course.setEndDate(new Date(System.currentTimeMillis()));
        CourseType courseType = new CourseType();
        courseType.setName("online");
        courseType.setId(1L);
        course.setType(courseType);
        return course;
    }

    @Test
    public void when_addCourse_expect_success() {
        omr.courses.domain.Course domainCourse = createDomainCourse("A great course about UML!!!");
        Course courseForDb = createCourseForDb("A great course about UML!!!");

        Mockito.when(courseRepository.save(ArgumentMatchers.eq(courseForDb)))
               .thenReturn(courseForDb);
        Mockito.when(courseTransformerToCourseForDb.transform(ArgumentMatchers.eq(domainCourse)))
               .thenReturn(courseForDb);
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseForDb)))
               .thenReturn(domainCourse);
        omr.courses.domain.Course addedCourse = courseCreationService.addCourse(domainCourse);
        
        Assertions.assertThat(addedCourse)
                  .isNotNull();
        Assertions.assertThat(addedCourse.getTitle())
                  .isEqualTo("A great course about UML!!!");
    }

    @Test(expected = CourseCreationException.class)
    public void when_addCourseWithDBError_expect_exception() {
        omr.courses.domain.Course domainCourse = createDomainCourse("A great course about UML!!!");
        Course courseForDb = createCourseForDb("A great course about UML!!!");
        Mockito.when(courseTransformerToCourseForDb.transform(ArgumentMatchers.eq(domainCourse)))
               .thenReturn(courseForDb);
        Mockito.doThrow(new CourseCreationException("Unable to create course"))
               .when(courseRepository)
               .save(ArgumentMatchers.any(Course.class));
        courseCreationService.addCourse(domainCourse);
    }
}
