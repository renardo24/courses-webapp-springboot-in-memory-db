package omr.courses.unit.service;

import omr.courses.domain.Course;
import omr.courses.exception.CourseNotFoundException;
import omr.courses.exception.CourseUpdateException;
import omr.courses.model.CourseType;
import omr.courses.repository.CourseRepository;
import omr.courses.service.CourseUpdateService;
import omr.courses.transformer.CourseTransformerToCourseForDb;
import omr.courses.transformer.CourseTransformerToCourseForDomain;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
public class CourseUpdateServiceTest {

    @MockBean
    private CourseRepository courseRepository;

    @MockBean
    private CourseTransformerToCourseForDb courseTransformerToCourseForDb;

    @MockBean
    private CourseTransformerToCourseForDomain courseTransformerToCourseForDomain;

    @InjectMocks
    private CourseUpdateService courseUpdateService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private omr.courses.domain.Course createDomainCourse(final String title) {
        omr.courses.domain.Course course = new omr.courses.domain.Course(title);
        course.setStartDate(LocalDate.now());
        course.setEndDate(LocalDate.now());
        course.setType(omr.courses.domain.CourseType.ONLINE);
        return course;
    }

    private omr.courses.model.Course createCourseForDb(final String title) {
        omr.courses.model.Course course = new omr.courses.model.Course();
        course.setId(1L);
        course.setTitle(title);
        course.setStartDate(new Date(System.currentTimeMillis()));
        course.setEndDate(new Date(System.currentTimeMillis()));
        CourseType courseType = new CourseType();
        courseType.setName("online");
        courseType.setId(1L);
        course.setType(courseType);
        return course;
    }

    @Test
    public void when_updateExistingCourse_expect_success() {
        omr.courses.domain.Course domainCourse = createDomainCourse("Some great course");
        omr.courses.model.Course courseForDb = createCourseForDb("Some great course");

        Mockito.when(courseRepository.save(ArgumentMatchers.eq(courseForDb)))
               .thenReturn(courseForDb);
        Mockito.when(courseTransformerToCourseForDb.transform(ArgumentMatchers.eq(domainCourse)))
               .thenReturn(courseForDb);
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseForDb)))
               .thenReturn(domainCourse);

        Course course = courseUpdateService.updateCourse(domainCourse);
        Assertions.assertThat(course)
                  .isNotNull();
        Assertions.assertThat(course.getTitle())
                  .isEqualTo("Some great course");
    }

    @Test(expected = CourseUpdateException.class)
    public void when_updateCourseThatDoesNotExist_expect_exception() {
        Course domainCourse = createDomainCourse("Some great course");
        omr.courses.model.Course courseForDb = createCourseForDb("Some great course");
        Mockito.when(courseTransformerToCourseForDb.transform(ArgumentMatchers.eq(domainCourse)))
               .thenReturn(courseForDb);
        Mockito.doThrow(new CourseNotFoundException("Course to update not found"))
               .when(courseRepository)
               .save(ArgumentMatchers.any(omr.courses.model.Course.class));
        courseUpdateService.updateCourse(domainCourse);
    }
}
