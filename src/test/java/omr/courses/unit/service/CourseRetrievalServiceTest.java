package omr.courses.unit.service;

import omr.courses.exception.CourseNotFoundException;
import omr.courses.exception.CourseRetrievalException;
import omr.courses.model.Course;
import omr.courses.model.CourseProvider;
import omr.courses.model.CourseType;
import omr.courses.repository.CourseRepository;
import omr.courses.service.CourseRetrievalService;
import omr.courses.transformer.CourseTransformerToCourseForDomain;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class CourseRetrievalServiceTest {

    @MockBean
    private CourseRepository courseRepository;

    @MockBean
    private CourseTransformerToCourseForDomain courseTransformerToCourseForDomain;

    @InjectMocks
    private CourseRetrievalService courseRetrievalService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private List<Course> createCourseList(final int listSize) {
        final List<Course> courses = new ArrayList<>();
        for (int i = 0; i < listSize; i++) {
            Course course = new Course();
            courses.add(createCourseFromDB(i, "Course #" + (i + 1)).get());
        }
        return courses;
    }

    private Optional<Course> createCourseFromDB(final long id, final String title) {
        Course course = new Course();
        course.setId(id);
        course.setTitle(title);
        course.setStartDate(new Date(System.currentTimeMillis()));
        course.setEndDate(new Date(System.currentTimeMillis()));
        CourseType courseType = new CourseType();
        courseType.setId(1L);
        courseType.setName("online");
        course.setType(courseType);
        CourseProvider courseProvider = new CourseProvider();
        courseProvider.setId(1L);
        courseProvider.setName("QA");
        course.setProvider(courseProvider);
        return Optional.of(course);
    }

    private omr.courses.domain.Course createDomainCourse(final Course courseFromDb) {
        omr.courses.domain.Course courseForDomain = new omr.courses.domain.Course();
        courseForDomain.setTitle(courseFromDb.getTitle());
        courseForDomain.setComments(courseFromDb.getComments());
        courseForDomain.setDuration(courseFromDb.getDuration());
        courseForDomain.setId(courseFromDb.getId());
        courseForDomain.setStartDate(courseFromDb.getStartDate()
                                                 .toLocalDate());
        courseForDomain.setEndDate(courseFromDb.getEndDate()
                                               .toLocalDate());
        courseForDomain.setInstructors(courseFromDb.getInstructors());
        courseForDomain.setLocation(courseFromDb.getLocation());
        courseForDomain.setProvider(courseFromDb.getProvider()
                                                .getName());
        courseForDomain.setType(omr.courses.domain.CourseType.fromString(courseFromDb.getType()
                                                                                     .getName()));
        return courseForDomain;
    }

    @Test
    public void when_getCoursesAndNoCoursesInDatabase_expect_success() {
        Mockito.when(courseRepository.findAll())
               .thenReturn(createCourseList(0));
        List<omr.courses.domain.Course> result = courseRetrievalService.getCourses();
        Assert.assertNotNull(result);
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void when_getCoursesAndOneCourseInDatabase_expect_success() {
        List<Course> courseList = createCourseList(1);
        Mockito.when(courseRepository.findAll())
               .thenReturn(courseList);
        omr.courses.domain.Course courseForDomain = createDomainCourse(courseList.get(0));
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseList.get(0))))
               .thenReturn(courseForDomain);
        List<omr.courses.domain.Course> result = courseRetrievalService.getCourses();
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("Course #1", result.get(0)
                                               .getTitle());
    }

    @Test
    public void when_getCoursesAndFourCoursesInDatabase_expect_success() {
        List<Course> courseList = createCourseList(4);
        Mockito.when(courseRepository.findAll())
               .thenReturn(courseList);
        omr.courses.domain.Course courseForDomain0 = createDomainCourse(courseList.get(0));
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseList.get(0))))
               .thenReturn(courseForDomain0);
        omr.courses.domain.Course courseForDomain1 = createDomainCourse(courseList.get(1));
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseList.get(1))))
               .thenReturn(courseForDomain1);
        omr.courses.domain.Course courseForDomain2 = createDomainCourse(courseList.get(2));
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseList.get(2))))
               .thenReturn(courseForDomain2);
        omr.courses.domain.Course courseForDomain3 = createDomainCourse(courseList.get(3));
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseList.get(3))))
               .thenReturn(courseForDomain3);
        List<omr.courses.domain.Course> result = courseRetrievalService.getCourses();
        Assert.assertNotNull(result);
        Assert.assertEquals(4, result.size());
        Assert.assertEquals("Course #1", result.get(0)
                                               .getTitle());
        Assert.assertEquals("Course #2", result.get(1)
                                               .getTitle());
        Assert.assertEquals("Course #3", result.get(2)
                                               .getTitle());
        Assert.assertEquals("Course #4", result.get(3)
                                               .getTitle());
    }

    @Test(expected = CourseNotFoundException.class)
    public void when_getCourseByIdNThrowsException_expect_exception() {
        Mockito.doThrow(new CourseNotFoundException("Course 456 Not found"))
               .when(courseRepository)
               .findById(ArgumentMatchers.anyLong());
        courseRetrievalService.getCourseById(456L);
    }

    @Test(expected = CourseNotFoundException.class)
    public void when_getCourseByIdNotInDatabase_expect_exception() {
        Mockito.when(courseRepository.findById(ArgumentMatchers.anyLong()))
               .thenReturn(Optional.empty());
        courseRetrievalService.getCourseById(456L);
    }

    @Test
    public void when_getCourseByIdInDatabase_expect_success() {
        Optional<Course> courseFromDb = createCourseFromDB(123L, "Course #1 Title");
        Mockito.when(courseRepository.findById(123L))
               .thenReturn(courseFromDb);
        omr.courses.domain.Course courseForDomain = createDomainCourse(courseFromDb.get());
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseFromDb.get())))
               .thenReturn(courseForDomain);
        omr.courses.domain.Course result = courseRetrievalService.getCourseById(123L);
        Assert.assertNotNull(result);
        Assert.assertEquals("Course #1 Title", result.getTitle());
    }

    @Test(expected = CourseRetrievalException.class)
    public void when_getCoursesAndTransformFails_expect_exception() {
        Mockito.doThrow(new NullPointerException())
               .when(courseTransformerToCourseForDomain)
               .transform(ArgumentMatchers.any(Course.class));
        List<Course> courseList = createCourseList(2);
        Mockito.when(courseRepository.findAll())
               .thenReturn(courseList);
        omr.courses.domain.Course courseForDomain0 = createDomainCourse(courseList.get(0));
        Mockito.when(courseTransformerToCourseForDomain.transform(ArgumentMatchers.eq(courseList.get(0))))
               .thenReturn(courseForDomain0);
        courseRetrievalService.getCourses();
    }

    @Test(expected = CourseRetrievalException.class)
    public void when_getCourseByIdAndTransformFails_expect_exception() {
        Mockito.doThrow(new NullPointerException())
               .when(courseTransformerToCourseForDomain)
               .transform(ArgumentMatchers.any(Course.class));
        Optional<Course> courseFromDb = createCourseFromDB(123L, "Course #1 Title");
        Mockito.when(courseRepository.findById(123L))
               .thenReturn(courseFromDb);
        courseRetrievalService.getCourseById(123L);
    }
}
