package omr.courses.unit.controller;

import omr.courses.controller.CourseDeletionController;
import omr.courses.domain.Course;
import omr.courses.exception.CourseDeletionException;
import omr.courses.service.CourseDeletionService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.ModelAndView;

@RunWith(SpringRunner.class)
public class CourseDeletionControllerTest {

    @MockBean
    private CourseDeletionService courseDeletionService;

    @InjectMocks
    private CourseDeletionController courseDeletionController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Course createCourse(final String title) {
        Course c = new Course(title);
        c.setId(1L);
        return c;
    }

    @Test
    public void when_deleteCourse_expect_success() {
        Mockito.doNothing()
               .when(courseDeletionService)
               .deleteCourseById(ArgumentMatchers.anyLong());
        ModelAndView modelAndView = courseDeletionController.deleteCourse(1L);
        Assertions.assertThat(modelAndView)
                  .isNotNull();
        Assertions.assertThat(modelAndView.getViewName())
                  .isEqualTo("redirect:/courses");
    }

    @Test(expected = CourseDeletionException.class)
    public void when_goToCoursePageAndNoCourseInDb_expect_failure() {
        Mockito.doThrow(new CourseDeletionException("No such course available"))
               .when(courseDeletionService)
               .deleteCourseById(Mockito.eq(1L));
        courseDeletionController.deleteCourse(1L);
    }
}
