package omr.courses.unit.controller;

import omr.courses.controller.CourseCreationController;
import omr.courses.domain.Course;
import omr.courses.domain.CourseValidator;
import omr.courses.exception.CourseCreationException;
import omr.courses.exception.CourseValidationException;
import omr.courses.service.CourseCreationService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

@RunWith(SpringRunner.class)
public class CourseCreationControllerTest {

    @MockBean
    private CourseCreationService courseCreationService;

    @MockBean
    private CourseValidator courseValidator;

    @MockBean
    private Model model;

    @InjectMocks
    private CourseCreationController courseCreationController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Course createCourse(final String title) {
        Course c = new Course(title);
        c.setId(1L);
        return c;
    }

    @Test
    public void when_goToAddCourseListPage_expect_success() {
        String page = courseCreationController.goToAddCoursePage(model);
        Assertions.assertThat(page)
                  .isNotEmpty();
        Assertions.assertThat(page)
                  .isEqualTo("course-form");
    }

    @Test
    public void when_addCourse_expect_success() {
        Mockito.doNothing()
               .when(courseValidator)
               .validateCourse(ArgumentMatchers.any(Course.class));
        ModelAndView modelAndView = courseCreationController.addCourse(createCourse("My Course"));
        Assertions.assertThat(modelAndView)
                  .isNotNull();
        Assertions.assertThat(modelAndView.getViewName())
                  .isEqualTo("redirect:/courses");
    }

    @Test(expected = CourseValidationException.class)
    public void when_validationFails_expect_failure() {
        Mockito.doThrow(new CourseValidationException("Invalid end date"))
               .when(courseValidator)
               .validateCourse(ArgumentMatchers.any(Course.class));
        courseCreationController.addCourse(createCourse("My Course"));
    }

    @Test(expected = CourseCreationException.class)
    public void when_errorAddingCourseToDb_expect_failure() {
        Mockito.doThrow(new CourseCreationException("Error adding course"))
               .when(courseCreationService)
               .addCourse(ArgumentMatchers.any(Course.class));
        courseCreationController.addCourse(createCourse("My Course"));
    }
}
