package omr.courses.unit.controller;

import omr.courses.controller.CourseRetrievalController;
import omr.courses.domain.Course;
import omr.courses.exception.CourseRetrievalException;
import omr.courses.service.CourseRetrievalService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;

import java.util.Collections;

@RunWith(SpringRunner.class)
public class CourseRetrievalControllerTest {

    @MockBean
    private CourseRetrievalService courseRetrievalService;

    @MockBean
    private Model model;

    @InjectMocks
    private CourseRetrievalController courseRetrievalController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Course createCourse(final String title) {
        Course c = new Course(title);
        c.setId(1L);
        return c;
    }

    @Test
    public void when_goToCourseListPageAndNoCoursesInDb_expect_success() {
        Mockito.when(courseRetrievalService.getCourses())
               .thenReturn(Collections.emptyList());
        String page = courseRetrievalController.goToCoursesListPage(model);
        Assertions.assertThat(page)
                  .isNotEmpty();
        Assertions.assertThat(page)
                  .isEqualTo("courses-list");
    }

    @Test
    public void when_goToCoursePage_expect_success() {
        Course course = createCourse("Some course");
        Mockito.when(courseRetrievalService.getCourseById(Mockito.eq(1L)))
               .thenReturn(course);
        String page = courseRetrievalController.goToCoursePage(1L, model);
        Assertions.assertThat(page)
                  .isNotEmpty();
        Assertions.assertThat(page)
                  .isEqualTo("course-details");
    }

    @Test(expected = CourseRetrievalException.class)
    public void when_goToCoursePageAndNoCourseInDn_expect_failure() {
        Mockito.doThrow(new CourseRetrievalException("No such course available"))
               .when(courseRetrievalService)
               .getCourseById(Mockito.eq(1L));
        courseRetrievalController.goToCoursePage(1L, model);
    }
}
