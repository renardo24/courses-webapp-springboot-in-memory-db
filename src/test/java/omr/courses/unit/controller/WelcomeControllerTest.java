package omr.courses.unit.controller;

import omr.courses.controller.WelcomeController;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class WelcomeControllerTest {

    @InjectMocks
    private WelcomeController welcomeController;

    @Test
    public void when_goToHomePage_expect_success() {
        String index = welcomeController.index();
        Assertions.assertThat(index)
                  .isNotEmpty();
        Assertions.assertThat(index)
                  .isEqualTo("index");
    }

    @Test
    public void when_goToWeclomePage_expect_success() {
        String welcome = welcomeController.welcome();
        Assertions.assertThat(welcome)
                  .isNotEmpty();
        Assertions.assertThat(welcome)
                  .isEqualTo("index");
    }
}
