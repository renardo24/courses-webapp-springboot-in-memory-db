package omr.courses.unit.controller;

import omr.courses.controller.CourseUpdateController;
import omr.courses.domain.Course;
import omr.courses.domain.CourseValidator;
import omr.courses.exception.CourseUpdateException;
import omr.courses.exception.CourseValidationException;
import omr.courses.service.CourseRetrievalService;
import omr.courses.service.CourseUpdateService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

@RunWith(SpringRunner.class)
public class CourseUpdateControllerTest {

    @MockBean
    private CourseUpdateService courseUpdateService;

    @MockBean
    private CourseRetrievalService courseRetrievalService;

    @MockBean
    private CourseValidator courseValidator;

    @MockBean
    private Model model;

    @InjectMocks
    private CourseUpdateController courseUpdateController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Course createCourse(final String title) {
        Course c = new Course(title);
        c.setId(1L);
        return c;
    }

    @Test
    public void when_goToUpdateCourseListPage_expect_success() {
        Mockito.when(courseRetrievalService.getCourseById(1L))
               .thenReturn(createCourse("My great course"));
        String page = courseUpdateController.goToUpdateCoursePage(1L, model);
        Assertions.assertThat(page)
                  .isNotEmpty();
        Assertions.assertThat(page)
                  .isEqualTo("course-form");
    }

    @Test
    public void when_addCourse_expect_success() {
        Mockito.doNothing()
               .when(courseValidator)
               .validateCourse(ArgumentMatchers.any(Course.class));
        ModelAndView modelAndView = courseUpdateController.updateCourse(createCourse("My Course"));
        Assertions.assertThat(modelAndView)
                  .isNotNull();
        Assertions.assertThat(modelAndView.getViewName())
                  .isEqualTo("redirect:/courses");
    }

    @Test(expected = CourseValidationException.class)
    public void when_validationFails_expect_failure() {
        Mockito.doThrow(new CourseValidationException("Invalid end date"))
               .when(courseValidator)
               .validateCourse(ArgumentMatchers.any(Course.class));
        courseUpdateController.updateCourse(createCourse("My Course"));
    }

    @Test(expected = CourseUpdateException.class)
    public void when_errorAddingCourseToDb_expect_failure() {
        Mockito.doThrow(new CourseUpdateException("Error adding course"))
               .when(courseUpdateService)
               .updateCourse(ArgumentMatchers.any(Course.class));
        courseUpdateController.updateCourse(createCourse("My Course"));
    }
}
