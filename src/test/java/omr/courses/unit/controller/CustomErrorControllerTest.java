package omr.courses.unit.controller;

import omr.courses.controller.CustomErrorController;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@RunWith(SpringRunner.class)
public class CustomErrorControllerTest {

    @MockBean
    private HttpServletRequest request;

    @InjectMocks
    private CustomErrorController customErrorController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void when_404_expect_404page() {
        Mockito.when(request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE))
               .thenReturn(404);
        String page = customErrorController.handleError(request);
        Assertions.assertThat(page)
                  .isEqualTo("error/error-404");
    }

    @Test
    public void when_400_expect_400page() {
        Mockito.when(request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE))
               .thenReturn(400);
        String page = customErrorController.handleError(request);
        Assertions.assertThat(page)
                  .isEqualTo("error/error-400");
    }

    @Test
    public void when_500_expect_500page() {
        Mockito.when(request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE))
               .thenReturn(500);
        String page = customErrorController.handleError(request);
        Assertions.assertThat(page)
                  .isEqualTo("error/error-500");
    }

    @Test
    public void when_anyStatusIsNull_expect_genericErrorPage() {
        Mockito.when(request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE))
               .thenReturn(null);
        String page = customErrorController.handleError(request);
        Assertions.assertThat(page)
                  .isEqualTo("error/error");
    }

    @Test
    public void when_anyOtherStatusCode_expect_genericErrorPage() {
        Mockito.when(request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE))
               .thenReturn(405);
        String page = customErrorController.handleError(request);
        Assertions.assertThat(page)
                  .isEqualTo("error/error");
    }

    @Test
    public void when_getErrorPath_expect_errorPathReturned() {
        Assertions.assertThat(customErrorController.getErrorPath())
                  .isEqualTo("/error");
    }

}
