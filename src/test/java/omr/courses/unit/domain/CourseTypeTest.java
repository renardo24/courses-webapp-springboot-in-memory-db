package omr.courses.unit.domain;

import omr.courses.domain.CourseType;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class CourseTypeTest {

    @Test
    public void when_courseTypeFromStringValid_expect_CourseTypeObject() {
        CourseType courseType = CourseType.fromString("online");
        Assertions.assertThat(courseType).isNotNull();
        Assertions.assertThat(courseType.getName()).isEqualTo("online");
    }

    @Test
    public void when_courseTypeFromStringInvalid_expect_null() {
        CourseType courseType = CourseType.fromString("someValue");
        Assertions.assertThat(courseType).isNull();

    }
}
