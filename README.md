# Courses Web App with Spring Boot and In-memory Database

Sprint boot application to manage training courses attended and stored
in a in-memory H2 database.

This application aims to show the various ways in which such courses
data can be managed through the web front-end:

* endpoints to retrieve all courses or a course by its id
* endpoint to delete a course by its id
* endpoint to add a brand new course
* endpoint to update an existing course

## Building the app

```bash
mvn clean package
```

## Running the app

```bash
mvn spring-boot:run
```

## Tests

Unit tests can be found in the `omr.courses.unit` package.
Integration tests can be found in the `omr.courses.integration` package.

Database integration tests can be found in the `omr.courses.integration.repository` package.
There are repository tests to add, delete, retrieve and update courses, course types
and course providers.

For the controllers, there are 2 types of integration tests:

* those that use `RestTemplate` and hence use a server (in the `omr.courses.integration.controller.resttemplate` package)
* those that use `MockMvc` without a server (in the `omr.courses.integration.controller.mockmvc_autoconfig` and
the `omr.courses.integration.controller.mockmvc_webmvctest` packages). The test in the `mockmvc_autoconfig` package
uses the `@AutoMvcConfig` annotation whereas those in the `mockmvc_webmvctest` use the `@WebMvctest` annotation. There
are no real differences between them since the `@webMvcTest` annotation includes the `@AutoMvcConfig` annotation already.
This was done for completeness.

There are the tests that I aimed to provide:

* [x] integration tests with rest templates &#x2611;
  * [x] all rest template integration tests in their own package (&#x2611; DONE)
  * [x] add (&#x2611; DONE)
  * [x] retrieve (&#x2611; DONE)
  * [x] update (&#x2611; DONE)
  * [x] delete (&#x2611; DONE) (but not really possible)
  * [x] welcome (&#x2611; DONE);
* [ ] repository integration tests for `Course` (in `omr.courses.integration.repository`):
  * [x] retrieve (DONE)
  * [x] delete (DONE)
  * [x] add (DONE) 
  * [ ] update
* [x] repository integration tests for `CourseType` (in `omr.courses.integration.repository`):
  * [x] retrieve (DONE)
  * [x] delete (DONE)
  * [x] add (DONE)
  * [x] update (DONE)
* [x] repository integration tests for `CourseProvider` (in `omr.courses.integration.repository`):
  * [x] retrieve (DONE)
  * [x] delete (DONE)
  * [x] add (DONE)
  * [x] update (DONE)
* [x] service classes integration test (in `omr.courses.integration.service`):
  * [x] retrieve (DONE)
  * [x] delete (DONE)
  * [x] add (DONE)
  * [x] update (DONE)
  * [x] a test that tests all four services all at once (DONE)
* [x] Try examples seen [here](https://spring.io/guides/gs/testing-web/), especially with these annotations
(towards the bottom of the page): `@RunWith(SpringRunner.class)`, `@SpringBootTest`, and `@AutoConfigureMockMvc`:
  * [x] retrieve (DONE)
  * [x] delete (DONE)
  * [x] add (DONE)
  * [x] update (DONE)
  * [x] customer error (DONE)
  * [x] welcome (DONE)
* [x] basic unit tests for controllers (in `omr.courses.unit.controller`)
  * [x] welcome (DONE)
  * [x] retrieve (DONE)
  * [x] delete (DONE)
  * [x] add (DONE)
  * [x] update (DONE)
  * [x] customer error (DONE)
* [x] basic unit tests for domain classes (in `omr.courses.unit.domain`)
* [x] basic unit tests for service classes (in `omr.courses.unit.service`)
* [x] basic unit tests for transformer classes (in `omr.courses.unit.transformer`) 

### Running tests

* `mvn test` (will run only the basic unit tests, and will stop the build if any of them fails),
* `mvn integration-test` (will run integration tests, and will not stop the build if any of them fails)
* `mvn verify` (will stop the build if an integration test fails)

### Test coverage

Run `mvn jacoco:report`. The results are available under `target/site/jacoco/index.html`.

---
